import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddtarjetaPage } from './addtarjeta.page';
var routes = [
    {
        path: '',
        component: AddtarjetaPage
    }
];
var AddtarjetaPageModule = /** @class */ (function () {
    function AddtarjetaPageModule() {
    }
    AddtarjetaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddtarjetaPage]
        })
    ], AddtarjetaPageModule);
    return AddtarjetaPageModule;
}());
export { AddtarjetaPageModule };
//# sourceMappingURL=addtarjeta.module.js.map