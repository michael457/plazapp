import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import 'rxjs/add/observable/combineLatest';
var RegistroPage = /** @class */ (function () {
    function RegistroPage(navCtrl, menuCtrl, auth, alertCtrl, db) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
    }
    RegistroPage.prototype.ngOnInit = function () { };
    RegistroPage.prototype.goLog = function () {
        this.navCtrl.navigateForward('log');
    };
    RegistroPage.prototype.goTerminos = function () {
        this.navCtrl.navigateForward('terminos');
    };
    RegistroPage.prototype.goIntro = function () {
        this.navCtrl.navigateBack('intro');
    };
    RegistroPage.prototype.signup = function () {
        var _this = this;
        var error = '';
        var hay_error = false;
        if (this.password.length < 6) {
            error = 'Debes ingresar una contraseña de al menos 3 caracteres';
            hay_error = true;
        }
        if (!this.validateEmail(this.email)) {
            error = 'Email inválido';
            hay_error = true;
        }
        if (this.tel.length < 7) {
            error = 'El número de teléfono debe tener más de 6 números';
        }
        if (this.name.length < 3) {
            error = 'Debes ingresar un nombre de al menos 3 caracteres';
            hay_error = true;
        }
        if (hay_error) {
            this.showAlert('Error de Registro', error);
        }
        else {
            var datos = {
                email: this.email,
                password: this.password,
                username: this.name,
                tel: this.tel
            };
            this.auth.doRegister(datos).then(function (resp) {
                console.log("Registrando:", resp);
                _this.navCtrl.navigateRoot("tabs/home");
            }).catch(function (err) {
                _this.showAlert('Error de Registro', "Contacta al administrador Error: 'Login FireEvent'");
            });
        }
    };
    RegistroPage.prototype.showAlert = function (titulo, mensaje) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertct;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: titulo,
                            message: mensaje,
                            buttons: ['OK']
                        })];
                    case 1:
                        alertct = _a.sent();
                        return [4 /*yield*/, alertct.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    RegistroPage = tslib_1.__decorate([
        Component({
            selector: 'app-registro',
            templateUrl: './registro.page.html',
            styleUrls: ['./registro.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, MenuController,
            AuthService, AlertController, AngularFireDatabase])
    ], RegistroPage);
    return RegistroPage;
}());
export { RegistroPage };
//# sourceMappingURL=registro.page.js.map