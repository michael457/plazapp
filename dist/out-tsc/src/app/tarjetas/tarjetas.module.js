import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TarjetasPage } from './tarjetas.page';
var routes = [
    {
        path: '',
        component: TarjetasPage
    }
];
var TarjetasPageModule = /** @class */ (function () {
    function TarjetasPageModule() {
    }
    TarjetasPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TarjetasPage]
        })
    ], TarjetasPageModule);
    return TarjetasPageModule;
}());
export { TarjetasPageModule };
//# sourceMappingURL=tarjetas.module.js.map