import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { UserService } from '../services/user-service';
var PayloadDonePage = /** @class */ (function () {
    function PayloadDonePage(navCtrl, menuCtrl, user, alertCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.user = user;
        this.alertCtrl = alertCtrl;
        this.nombredia = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];
    }
    PayloadDonePage.prototype.ngOnInit = function () {
    };
    PayloadDonePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //Get User
        this.user.getCurrentUser()
            .then(function (user) {
            _this.username = user.name;
        });
        //Get Dir
        var diractual = parseInt(localStorage.getItem("address"));
        var dirs = JSON.parse(localStorage.getItem('list-address'));
        var dirselect = dirs.filter(function (item) { return item.id === diractual; })[0];
        this.dir = dirselect.direccion;
        //Get Range Time
        var rang = localStorage.getItem("range-time");
        var rangsplit = rang.split("|");
        var dia = new Date(rangsplit[0]);
        var ini = new Date(rangsplit[1]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        var fini = new Date(rangsplit[2]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        this.range = this.nombredia[dia.getDay()] + " " + ini + " a " + fini;
    };
    PayloadDonePage.prototype.goBack = function () {
        this.navCtrl.navigateRoot("tabs/home");
    };
    PayloadDonePage = tslib_1.__decorate([
        Component({
            selector: 'app-payload-done',
            templateUrl: './payload-done.page.html',
            styleUrls: ['./payload-done.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            UserService,
            AlertController])
    ], PayloadDonePage);
    return PayloadDonePage;
}());
export { PayloadDonePage };
//# sourceMappingURL=payload-done.page.js.map