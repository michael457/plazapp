import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
var RecetasPage = /** @class */ (function () {
    function RecetasPage(navCtrl, param, auth, db) {
        this.navCtrl = navCtrl;
        this.param = param;
        this.auth = auth;
        this.db = db;
        this.sliderConfig = {
            slidesPerView: 3,
            spaceBetween: 10,
            centeredSlides: false
        };
        this.product = [
            { title: 'Frutal', img: 'receta1.jpg' },
            { title: 'Tropical', img: 'receta1.jpg' },
            { title: 'Detox', img: 'receta1.jpg' }
        ];
    }
    RecetasPage.prototype.ngOnInit = function () {
    };
    RecetasPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.param.serviceData
            .subscribe(function (product) { return (_this.productoid = product); });
        console.log("Recibiendo productoid: ", this.productoid);
        this.getPost();
    };
    RecetasPage.prototype.getPost = function () {
        var _this = this;
        var me = this;
        this.auth.getGenOV('Post').then(function (data) {
            //me.post = data;
            var posts;
            posts = data;
            _this.post = posts.filter(function (item) { return item.producto === _this.productoid; });
            localStorage.setItem('recetas', JSON.stringify(data));
            console.log("Post: ", _this.post);
        });
    };
    RecetasPage.prototype.goReceta = function (id) {
        this.param.dataPost(id);
        console.log("Post:", id);
        this.navCtrl.navigateForward("receta");
    };
    RecetasPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    RecetasPage = tslib_1.__decorate([
        Component({
            selector: 'app-recetas',
            templateUrl: './recetas.page.html',
            styleUrls: ['./recetas.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, ParametrosService, AuthService, AngularFireDatabase])
    ], RecetasPage);
    return RecetasPage;
}());
export { RecetasPage };
//# sourceMappingURL=recetas.page.js.map