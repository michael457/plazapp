import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddproductoPage } from './addproducto.page';
var routes = [
    {
        path: '',
        component: AddproductoPage
    }
];
var AddproductoPageModule = /** @class */ (function () {
    function AddproductoPageModule() {
    }
    AddproductoPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddproductoPage]
        })
    ], AddproductoPageModule);
    return AddproductoPageModule;
}());
export { AddproductoPageModule };
//# sourceMappingURL=addproducto.module.js.map