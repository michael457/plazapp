import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ParametrosService } from '../services/parametros.service';
var BlogPage = /** @class */ (function () {
    function BlogPage(navCtrl, menuCtrl, auth, param) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.param = param;
    }
    BlogPage.prototype.ngOnInit = function () {
    };
    BlogPage.prototype.ionViewWillEnter = function () {
        this.getPost();
    };
    BlogPage.prototype.getPost = function () {
        var _this = this;
        var me = this;
        this.auth.getGenOV('Post').then(function (data) {
            var posts;
            posts = data;
            _this.post = posts.filter(function (item) { return item.categoria === 4; });
            localStorage.setItem('blog', JSON.stringify(data));
            console.log("Post: ", _this.post);
        });
    };
    BlogPage.prototype.goEntrada = function (id) {
        this.param.dataPost(id);
        this.navCtrl.navigateForward("entrada-blog");
    };
    BlogPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    BlogPage = tslib_1.__decorate([
        Component({
            selector: 'app-blog',
            templateUrl: './blog.page.html',
            styleUrls: ['./blog.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, MenuController, AuthService, ParametrosService])
    ], BlogPage);
    return BlogPage;
}());
export { BlogPage };
//# sourceMappingURL=blog.page.js.map