import * as tslib_1 from "tslib";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ShopPage } from './shop.page';
import { ComponentsModule } from '../componentes/componentes.module';
var routes = [
    {
        path: '',
        component: ShopPage
    }
];
var ShopPageModule = /** @class */ (function () {
    function ShopPageModule() {
    }
    ShopPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ComponentsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ShopPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], ShopPageModule);
    return ShopPageModule;
}());
export { ShopPageModule };
//# sourceMappingURL=shop.module.js.map