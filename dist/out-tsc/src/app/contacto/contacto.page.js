import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { MenuController } from '@ionic/angular';
var CONTACTOPage = /** @class */ (function () {
    function CONTACTOPage(callNumber, menuCtrl) {
        this.callNumber = callNumber;
        this.menuCtrl = menuCtrl;
    }
    CONTACTOPage.prototype.ngOnInit = function () {
    };
    CONTACTOPage.prototype.openPhone = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    CONTACTOPage.prototype.openWhatsapp = function (tel) {
        window.open("whatsapp://send?text=Hola,+solicito+soporte&phone=" + tel, "_system", "location=yes");
    };
    CONTACTOPage.prototype.openEmail = function () {
        window.open('mailto:plaz@gmail.com', "_system");
    };
    CONTACTOPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    CONTACTOPage = tslib_1.__decorate([
        Component({
            selector: 'app-contacto',
            templateUrl: './contacto.page.html',
            styleUrls: ['./contacto.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CallNumber, MenuController])
    ], CONTACTOPage);
    return CONTACTOPage;
}());
export { CONTACTOPage };
//# sourceMappingURL=contacto.page.js.map