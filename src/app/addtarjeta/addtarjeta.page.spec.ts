import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtarjetaPage } from './addtarjeta.page';

describe('AddtarjetaPage', () => {
  let component: AddtarjetaPage;
  let fixture: ComponentFixture<AddtarjetaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtarjetaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtarjetaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
