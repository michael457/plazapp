import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropiedadesPage } from './propiedades.page';

describe('PropiedadesPage', () => {
  let component: PropiedadesPage;
  let fixture: ComponentFixture<PropiedadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropiedadesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropiedadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
