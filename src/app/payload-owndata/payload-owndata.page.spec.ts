import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayloadOwndataPage } from './payload-owndata.page';

describe('PayloadOwndataPage', () => {
  let component: PayloadOwndataPage;
  let fixture: ComponentFixture<PayloadOwndataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayloadOwndataPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayloadOwndataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
