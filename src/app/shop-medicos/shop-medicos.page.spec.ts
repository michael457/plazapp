import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShopMedicosPage } from './shop-medicos.page';

describe('ShopMedicosPage', () => {
  let component: ShopMedicosPage;
  let fixture: ComponentFixture<ShopMedicosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopMedicosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ShopMedicosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
