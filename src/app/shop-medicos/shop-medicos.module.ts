import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';


import { ShopMedicosPage } from './shop-medicos.page';


const routes: Routes = [
  {
    path: '',
    component: ShopMedicosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ShopMedicosPage]
})
export class ShopMedicosPageModule {}
