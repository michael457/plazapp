import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayloadProgramPage } from './payload-program.page';

describe('PayloadProgramPage', () => {
  let component: PayloadProgramPage;
  let fixture: ComponentFixture<PayloadProgramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayloadProgramPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayloadProgramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
