import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CONTACTOPage } from './contacto.page';

describe('CONTACTOPage', () => {
  let component: CONTACTOPage;
  let fixture: ComponentFixture<CONTACTOPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CONTACTOPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CONTACTOPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
