import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NavController, Platform, LoadingController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';



@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class CONTACTOPage implements OnInit {

  chat_enable = "1";
  telefono = '';
  whatsappOpen: string = '';
  whatsapp = "Sin Whatsapp";
  mailOpen: string = '';
  phoneOpen: string = '';
  paisCodigo: String = '+57';
  urlWhatsapp: String = 'https://wa.me/';
  platformActual;

  constructor(private callNumber: CallNumber,
    public menuCtrl: MenuController,
    private nav: NavController,
    private auth: AuthService,
    public platform: Platform) {  
  }

  ngOnInit() {
   
  }

  ionViewWillEnter() {    
    const ciudad = localStorage.ciudad;

    //Get setting for contact
    let tabla = "UnidadNegocio_" + localStorage.UnidadNegocio + "/Settings";

    this.auth.getGenKeyVal(tabla, "llave", "whatsapp_contacto").then(data => {
      let indata: any;
      indata = data;      
      this.whatsappOpen = "https://api.whatsapp.com/send?phone="+indata[0].valor+"&text=&source=&data=&app_absent=";      
      console.log("Whatsapp ", this.whatsappOpen);      
    });

    this.auth.getGenKeyVal(tabla, "llave", "email_contacto").then(data => {
      let indata: any;
      indata = data;
      this.mailOpen = 'mailto:' + indata[0].valor + '?Subject=Soporte Plaz';
      console.log("Email ", this.mailOpen);
    });

    this.auth.getGenKeyVal(tabla, "llave", "telefono_contacto").then(data => {
      let indata: any;
      indata = data;
      this.telefono = indata[0].valor;
      this.phoneOpen = 'tel:+57' + indata[0].valor;
      console.log("Telefono ", this.phoneOpen);
    });
  }

  openPhone(phone: string) {
    this.callNumber.callNumber(phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  llamar() {
    window.open(`tel:+57${this.telefono}`, '_system');
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  enviarEmail() {
    window.open(`${this.mailOpen}`, '_system')
  }

  openWhatsapp(tel: String){        
    window.open("https://api.whatsapp.com/send?phone="+this.whatsappOpen+"&text=&source=&data=&app_absent=",'_system');
    console.log("https://api.whatsapp.com/send?phone="+this.whatsappOpen+"&text=&source=&data=&app_absent=");
  }


}
