import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-mis-creditos',
  templateUrl: './mis-creditos.page.html',
  styleUrls: ['./mis-creditos.page.scss'],
})
export class MisCreditosPage implements OnInit {
  
  codigo;
  valorReferido = 0;
  referido = true;
  unidadNegocio: string = localStorage.getItem('UnidadNegocio');
  frutiCoins: string;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController, 
              private auth: AuthService,
              public param: ParametrosService,
              public server : ServerService,
              private socialSharing: SocialSharing) { }

  ngOnInit() {
  }
  
  ionViewWillEnter() {    
    this.frutiCoins = localStorage.getItem("creditos");
    const uid = localStorage.uid;
    this.Creditos(uid);    
    this.codigo = uid.substr(5,8);
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla, "llave", "credito_referidos").then(data => {      
      this.valorReferido = data[0]['valor'];
    }).catch(error => {
      console.log(error);
    });
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  Creditos(uid){
    this.server.getFruticoins(uid).then( data => {
      if(data['ok']  && data['Fruticoins']) {
        this.frutiCoins = data['Fruticoins'][0]['Valor'];          
        localStorage.setItem( "creditos", (this.frutiCoins) );
      } else {
        this.frutiCoins = "0";
        localStorage.setItem( "creditos", "0" );         
      }
    });
  }

  sendShare() {
    let message = "Hola, regístrate en Plaz con este codigo "+this.codigo+" y obten $"+this.valorReferido+" para tu primer mercado. 🍐🍎🍇🍅🥬"
    let subject = "Un regalo de PLAZ";
    let url = "https://somosplaz.com/";
    this.socialSharing.share(message, subject, null, url);
  }

  

}
