import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Events} from '@ionic/angular';
import {observable, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';
import {UserService} from "./user-service";
import {firebaseConfig} from '../../config';
import {UniqueDeviceID} from "@ionic-native/unique-device-id/ngx";
//import * as jwt from 'jsonwebtoken';

declare var zChat;

export class ChatMessage {
  messageId: string;
  userId: string;
  userName: string;
  userAvatar: string;
  toUserId: string;
  time: number | string;
  message: string;
  status: string;
}

export class UserInfo {
  id: string;
  name?: string;
  avatar?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public CHAT_STATUS = {
    NO_INICIADO: "NO_INICIADO",
    CONECTANDO: "CONECTANDO",
    CONECTADO: "CONECTADO"
  };
  private messages: ChatMessage[];
  private currentUser: UserInfo;
  private status = this.CHAT_STATUS.NO_INICIADO;
  private deviceId: any;

  constructor(private http: HttpClient,
              private events: Events, 
              private userService: UserService, 
              private uniqueDeviceID: UniqueDeviceID) {

    this.uniqueDeviceID.get()
      .then((uuid: any) => this.deviceId = uuid)
      .catch((error: any) => {
        let id = localStorage.getItem("deviceId") || this.generateUUID();
        localStorage.setItem("deviceId", id);
        this.deviceId = id;
      });

  }

  private generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  inciarChat() {
    if (this.status != this.CHAT_STATUS.NO_INICIADO) return;
    this.status = this.CHAT_STATUS.CONECTANDO;
    this.events.publish('chat:connection_update', this.status, Date.now());
    const self = this;
    var visitorInfo = {
      display_name: this.currentUser.name,
      email: this.currentUser.name.indexOf("@") >= 0 ? this.currentUser.name : this.currentUser.id + "@anonimo.co"
    };

    //zChat.init({
    //  account_key: firebaseConfig.zendeskKey,
    //  authentication: {
    //    jwt_fn: function (callback) {
    //      var data = {
    //        name: visitorInfo.display_name,
    //        email: visitorInfo.email,
    //        //phone: parseInt(body.phone),
    //        external_id: self.currentUser.id,
    //        //exp: Math.floor(Date.now() / 1000) + (60 * 5)
    //      }
    //      var token = jwt.sign(data, firebaseConfig.zendeskSecret, {
    //        expiresIn: "15s",
    //        "header": {"alg": 'HS256', "cty": "JWT", "typ": undefined}
    //      });
    //      console.log("JWT", token);
    //      callback(token);
    //    }
    //  }
    //});

    zChat.on('connection_update', function (status) {
      self.onConnectionUpdate(status);
    });

    zChat.on('chat', function (data) {
      self.onChat(data);
    })
  }

  onConnectionUpdate(status) {
    if (status === 'connected') {
      // Start consuming your API here
      console.log("Connected !!");
      this.status = this.CHAT_STATUS.CONECTADO;
      this.events.publish('chat:connection_update', this.status, Date.now());
      const welcomeMsg: ChatMessage = {
        messageId: Date.now().toString(),
        userId: 'agente007',
        userName: 'PLAZ',
        userAvatar: './assets/imgs/plaz-user.png',
        toUserId: this.currentUser.id,
        time: Date.now(),
        message: "Hola, cuentanos en que te podemos ayudar.",
        status: 'success'
      };
      if (this.messages.length == 0) {
        // this.events.publish('chat:received', welcomeMsg, Date.now())
      }
    }
  }

  onChat(data) {
    console.log(data);
    if (data.type == 'chat.msg') {
      let fromAgent = data.nick.indexOf("agent:") >= 0;
      const msg: ChatMessage = {
        messageId: data.timestamp,
        userId: fromAgent ? "agente007" : this.currentUser.id,
        userName: fromAgent ? data.display_name : 'Tú'/*this.currentUser.name*/,
        userAvatar: fromAgent ? './assets/imgs/plaz-user.png' : this.currentUser.avatar,
        toUserId: fromAgent ? this.currentUser.id : "agente007",
        time: data.timestamp,
        message: data.msg,
        status: 'success'
      };
      this.events.publish('chat:received', msg, Date.now());
    }
  }


  getMsgList(): Observable<ChatMessage[]> {

    console.log("Chat Service: getMsgList")
    if (!this.messages) {
      /* const msgListUrl = './assets/mock/msg-list.json';
       return this.http.get<any>(msgListUrl)
         .pipe(map(response => {
             this.messages = response.array;
             return this.messages;
           }
         ));*/
      this.messages = [];
    }
    return of(this.messages);
  }

  sendMsg(msg: ChatMessage) {
    const self = this;
    return new Promise(resolve => {
      zChat.sendChatMsg(msg.message, function (err) {

        console.log("zChat.sendChatMsg callback", err);
        if (err) {
          // TODO Manejar los errores
          console.error(err);
        } else {
          console.log("Mensaje enviado!", self.messages);
          //self.messages.push(msg);
          resolve();
        }

      })
    });
  }

  getUserInfo(): Promise<UserInfo> {
    if (!this.currentUser) {

      return new Promise(resolve => {
        this.userService.getCurrentUser()
          .then(user => {
            console.log("traer el usuario del user service", user);
            if (user.provider == "password") {
              user.name = user.email;
            }
            const userInfo: UserInfo = {
              id: user.uid,
              name: user.name,
              avatar: user.image
            };
            this.currentUser = userInfo;
            resolve(userInfo)
          }).catch(err => {
          const userInfo: UserInfo = {
            id: "ANON." + this.deviceId,
            name: "Anónimo",
            avatar: './assets/user.jpg'
          };
          this.currentUser = userInfo;
          resolve(userInfo)
        });
      });
    }
    return new Promise(resolve => resolve(this.currentUser));
  }

}

