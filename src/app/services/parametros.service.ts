import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  	parametroCategoria: any;

  	constructor() { }
  
  	private dataSource = new BehaviorSubject("default message");
    serviceData = this.dataSource.asObservable();

    changeData(data: any) {
      this.dataSource.next(data);
    }
    dataProducto(product: any) {
      this.dataSource.next(product);
    }
    dataPost(post: any) {
      this.dataSource.next(post);
    }
}
