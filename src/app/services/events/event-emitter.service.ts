import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  constructor() { }

  sendEmit(param: boolean = false) {
    this.eventEmitter.emit(param);
  }
}
