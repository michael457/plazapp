import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecetasMedicosPage } from './recetas-medicos.page';

const routes: Routes = [
  {
    path: '',
    component: RecetasMedicosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecetasMedicosPageRoutingModule {}
