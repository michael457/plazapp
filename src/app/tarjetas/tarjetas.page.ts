import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ServerService } from "../server/server.service";
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.page.html',
  styleUrls: ['./tarjetas.page.scss'],
})
export class TarjetasPage {
  fromCartNav = false;
	tarjetas: any;
  datos = {
    "uid": "",
    "id": "",
    "ultimos": ""
  };
  sinTarjetas = false;
  titulo;
  constructor(public navCtrl: NavController, 
    private auth: AuthService, 
    public server: ServerService, 
    private user: UserService) { }

  async ionViewWillEnter() {
    console.log('Entro');
    let fromCart = localStorage.getItem("fromCart");
    if (fromCart=="true") {
      this.fromCartNav = true;
    }
    this.getTarjeta();  
  }

  goBack() {
    this.navCtrl.navigateBack("tabs/home");
  }

  goBackT() {
    this.navCtrl.navigateBack("tipo-pago");
  }

  goAddTarjeta() {
    this.navCtrl.navigateForward("addtarjeta");
  }

  Selection(item: any) {
	   this.tarjetas.forEach(x => { x.checked = false });
     localStorage.setItem("tarjeta_actual",item.id);
     localStorage.setItem("tarjeta_nombre",item.ultimos+"|"+item.tipoTarjeta);
     let fromCart = localStorage.getItem("fromCart");
     if (fromCart=="true") {
        //this.navCtrl.navigateBack("payload-checkout");
        this.navCtrl.navigateForward("tipo-pago");
    }
	}

  async getTarjeta() {
    let uid = localStorage.uid; 
    await this.server.GetTarjetas(uid).then( data => {
      console.log('data ', data);
      let tarjetas : any = data;
      if(tarjetas.length === 0){
        this.sinTarjetas = true;
        this.titulo = "Aún no haz guardado ninguna tarjeta";
        this.tarjetas = null;
        return;
      }
      else{
        this.sinTarjetas = false;
        this.tarjetas = data;        
        this.titulo = "Estas son las tarjetas que haz guardado";  
        let actualtarjeta = localStorage.getItem("tarjeta_actual") || 0;
        console.log("Tarjeta Actual",actualtarjeta);
        for (var i = 0; i < this.tarjetas.length; i++) {
          if(this.tarjetas[i].id==actualtarjeta) {
            this.tarjetas[i].checked = true; 
          } else {
            this.tarjetas[i].checked = false;
          }
        }
      }
    }).catch(error => {
      console.log(error);
    })
  }

  deleteTarjeta( tarjeta: any ){

    console.log('tarjeta ', tarjeta.ultimos);

    this.server.presentLoadingDefault("Eliminando...");
    //let user = this.auth.getUser();
    this.datos.uid = localStorage.uid;
    this.datos.id = tarjeta.id;
    this.datos.ultimos = tarjeta.ultimos;
    this.server.removeTarjeta(this.datos).then(async (data)=>{      
      if(data['ok']) {        
        await this.getTarjeta();
        this.server.showAlert("Tarjeta Eliminada","Tarjeta eliminada con éxito");
        this.server.dismissLoading();
      } else {
        this.server.dismissLoading();
        this.server.showAlert("Error al Eliminar","Ha ocurrido un error al eliminar tarjeta");
      }       
    });
  }
}
