import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntradaBlogPage } from './entrada-blog.page';

describe('EntradaBlogPage', () => {
  let component: EntradaBlogPage;
  let fixture: ComponentFixture<EntradaBlogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntradaBlogPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntradaBlogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
