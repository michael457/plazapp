import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';
import { UserService } from '../services/user-service';


@Component({
  selector: 'app-micuenta',
  templateUrl: './micuenta.page.html',
  styleUrls: ['./micuenta.page.scss'],
})
export class MicuentaPage implements OnInit {
	user;
  userimg;
  name;
  tel;
  email;
  pass;
  social;
  tarjetas;
  constructor(private auth: AuthService,
    public menuCtrl: MenuController,
    public server: ServerService,
    public nav: NavController,
    public userService: UserService,
    //private useru: UserService,
    public navCtrl: NavController) {
  	// this.userService.getCurrentUser()
    // .then(user => {
    //   console.log(user);
    // this.name = user.name;
    // this.email = user.email;
    // this.tel = user.telefono;
    // this.userimg = user.image;
    // if (user.provider =='facebook.com') {
    //   this.social = 'facebook';
    //   this.pass = 'social';
    // } else if (user.provider == "google.com") {
    //   this.social = 'google';
    //   this.pass = 'social';
    // } else {
    //   this.social = 'none';
    //   this.pass = '1234';
    // }
    //console.log(user);
    //});
  }

  ngOnInit() {}

  async ionViewWillEnter() {
    localStorage.setItem('fromCart', "false");
    await this.UserInfo();
    await this.Tarjetas();
  }

  Tarjetas(){
    var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla, "llave", "agrega_tarjetas").then(data => {
      this.tarjetas = String(data[0].valor);
    })
  }

  UserInfo(){
    let user = JSON.parse(localStorage.user);
    this.email = user.email;
    this.name = user.nombre;
    this.tel = user.telefono;
    this.userimg = 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/user.svg?alt=media&token=593d7587-8a14-4431-b8a2-2f1724945335';
    this.pass = "123456789";
  }

  UserInfo2()
  {
    let userService = this.userService.getCurrentUser();
    this.userService.getCurrentUser().then(user => {
    this.server.getInfoUser(user.uid).then((data)=>{
      let resp: any = data;
      if(resp.ok){
        let usuario = resp.uid[0];
        console.log("Usuariooo", usuario);
        this.name = usuario.nombre;
        this.email = usuario.email;
        this.tel = usuario.telefono;

        if (usuario.provider_data =='facebook.com') {
          this.social = 'facebook';
          this.pass = 'social';
        } else if (usuario.provider_data == "google.com") {
          this.social = 'google'; 
          this.pass = 'social';
        } else {
          this.social = 'none';
          this.pass = '1234';
        }
      } else{
        this.server.showAlert('Error Servidor',"No se pueden obtener los pedidos.");
      }
      }).catch((err)=>{
          this.server.showAlert('Error Red',"No se pudo conectar con el servidor.");
      });
    });
  }

  actualizarUsuario() {
    let telef = this.tel+"";
    if(telef.length != 10)
    {
      console.log(telef.length);
      this.server.showAlert('Mensaje', "Debes ingersar un número celular válido (10 dígitos) sin espacios y sin indicativo.");
      return;
    }

    this.userService.getCurrentUser()
    .then(user => {
      if (user.provider == "facebook.com" || user.provider =='google.com') {
        user.telefono = this.tel;
      } else {
        user.telefono = this.tel;
        user.name = this.name;
      }
      console.log(user);
      this.server.updateUser(user).then((data) => {
        this.server.showAlert('Usuario Actualizado', "Usuario actualizado con éxito");
      }).catch((err) => {
        this.server.showAlert('Error al Actualizar','Ha ocurrido un error al actualizar la información del usuario ');
      });
    });
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  goTarjetas() {
    this.nav.navigateForward('tarjetas');
  }

  logout() {
    this.auth.signOut();
    this.nav.navigateRoot('intro');
  }

  gotorecovery() {
    this.nav.navigateBack('repass');
  }

  fbLogin() {
    console.log('Facebook Login Reconnect');
    this.auth.signOut();
    this.auth.doFacebookLogin().then((resp) => {
      console.log('Autenticado:', resp);
      this.navCtrl.navigateRoot('tabs/home');
    }).catch((err) => {
      this.server.showAlert('Error al Ingresar','Error al ingresar con Facebook, intente nuevamente.');
      console.log('error:', err);
    });
  }

  googleLogin() {
    console.log('Google Login Reconnect');
    this.auth.signOut();
    this.auth.doGoogleLogin().then((resp) => {
      console.log('Autenticado:', resp);
        this.navCtrl.navigateRoot('tabs/home');
      }).catch((err) => {
      this.server.showAlert('Error al Ingresar', "Error al ingresar con Google, intente nuevamente.");
      console.log('error:', err);
    });
  }



}
