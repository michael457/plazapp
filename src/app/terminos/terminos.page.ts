import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { HTTP } from '@ionic-native/http/ngx';
import { ServerService } from '../server/server.service';

@Component({
  selector: 'app-terminos',
  templateUrl: './terminos.page.html',
  styleUrls: ['./terminos.page.scss'],
})
export class TerminosPage implements OnInit {
  terminos="<img src='assets/imgs/load.gif' style='width:100%;'>";
  error = "<h3 class='title_error'>Error al cargar términos.</h3><p><div class='error_text'>Intenta nuevamente</div></p>";
  constructor(public navCtrl: NavController,private auth: AuthService,public http: HTTP,private server: ServerService) {
    /* Carga Terminos */
    this.server.getTerminos().then((data)=>{
        let resp: any = data;
        if(resp.ok){
          this.terminos = resp.terminos;
        } else{
          this.terminos = this.error;
          this.server.showAlert('Error Servidor',"No se puedo obtener los términos desde el servidor.");
        }
    }).catch((err)=>{
        this.terminos = this.error;
        this.server.showAlert('Error Red',"No se pudo conectar con el servidor.");
    });
  }
  ngOnInit() {}
  /* Ir a Intro */
  goIntro(){
    this.navCtrl.navigateBack('intro');
  }

}
