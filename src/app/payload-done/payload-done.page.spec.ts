import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayloadDonePage } from './payload-done.page';

describe('PayloadDonePage', () => {
  let component: PayloadDonePage;
  let fixture: ComponentFixture<PayloadDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayloadDonePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayloadDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
